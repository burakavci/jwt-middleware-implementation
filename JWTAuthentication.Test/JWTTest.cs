using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using Microsoft.AspNetCore.Mvc.Testing;
using Xunit;

namespace JWTAuthentication.Test
{
    public class JWTTest : IClassFixture<WebApplicationFactory<API.Startup>>
    {
        private HttpClient _httpClient;
        
        public JWTTest(WebApplicationFactory<API.Startup> api)
        {
            _httpClient = api.CreateClient();
        }

        [Fact]
        public void NoBearerTest()
        {
            var res = _httpClient.GetAsync("/weatherforecast").Result;
            Assert.Equal(HttpStatusCode.BadRequest, res.StatusCode);
        }

        [Fact]
        public void SignatureMismatchTest()
        {
            var realJwt =
                "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.1KZMskqLY0IV5m45BfYJP7Wp-GvCUGJo-hJKX3LSGDo";
            var infectedJwt =
                "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIzfQ.1KZMskqLY0IV5m45BfYJP7Wp-GvCUGJo-hJKX3LSGDo";
            var res = _httpClient.SendAsync(new HttpRequestMessage(HttpMethod.Get, "/weatherforecast"){ Headers = { Authorization = AuthenticationHeaderValue.Parse("Bearer " + infectedJwt)}}).Result;
            Assert.Equal(HttpStatusCode.Unauthorized, res.StatusCode);
        }
        
        [Fact]
        public void CorrectJWT()
        {
            var realJwt =
                "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIzLCJleHAiOjE3NTE3MjU0NjZ9.uQRsGipiR5TqGudGAtmdhD07pxJigMfLSclY7jTWvGI";
            var res = _httpClient.SendAsync(new HttpRequestMessage(HttpMethod.Get, "/weatherforecast"){ Headers = { Authorization = AuthenticationHeaderValue.Parse("Bearer " + realJwt)}}).Result;
            Assert.Equal(HttpStatusCode.OK, res.StatusCode);
        }
        
        [Fact]
        public void ExpiredJWT()
        {
            var realJwt =
                "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIzLCJleHAiOjE1MTYyMzkwMjN9.E_gsccxzW52o_0hnyc5cpLIApp-5MCH8hmOiN6amU0s";
            var res = _httpClient.SendAsync(new HttpRequestMessage(HttpMethod.Get, "/weatherforecast"){ Headers = { Authorization = AuthenticationHeaderValue.Parse("Bearer " + realJwt)}}).Result;
            Assert.Equal(HttpStatusCode.Unauthorized, res.StatusCode);
        }
    }
}