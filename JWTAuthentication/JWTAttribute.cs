using System;
using Swashbuckle;

namespace JWTAuthentication
{
    public class JWTAttribute : Attribute
    {
        public string Password { get; set; }
        public JWTAttribute(string password)
        {
            Password = password;
        }
    }
}